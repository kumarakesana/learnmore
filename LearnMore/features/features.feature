@Test
Feature: Jqueryui Test

  Background: Test
    Given I will navigate to webpage

  Scenario Outline: Testing Draggable
    When I enter "<name>"
    Then I will see the action

    Examples: 
      | name      |
      | Draggable |

  Scenario Outline: Testing Droppable
    When I enter the "<dropp>"
    Then I will see drag and drop

    Examples: 
      | dropp     |
      | Droppable |
