package object.repo;

import org.openqa.selenium.By;

public class Locators {
	
	public static By drag = By.id("draggable");
	public static By drop = By.id("droppable");
	public static By resize = By.xpath("//*[@id=\"resizable\"]/div[3]");
	public static By cone = By.xpath("//*[@id=\"selectable\"]/li[1]");
	public static By ctwo = By.xpath("//*[@id=\"selectable\"]/li[2]");
	public static By site = By.id("site-select");
	public static By adviser = By.partialLinkText("Login");
	public static By user = By.id("txtusername");
	public static By pass = By.id("txtpassword");
	public static By clk = By.id("btnLogin");
}
