package utils.pack;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GenericLibrary {
	
	public static void click (final WebDriver driver, By locator)
	{
		driver.findElement(locator).click();
	}
	
	public static void text (final WebDriver driver, By locator, String text)
	{
		driver.findElement(locator).sendKeys(text);
	}
	
	public static void implicit(final WebDriver driver, int i)
	{
		driver.manage().timeouts().implicitlyWait(i, TimeUnit.SECONDS);
	}
	
	public static void explicitclick (final WebDriver driver, int j, By locator)
	{
		WebDriverWait explicit = new WebDriverWait(driver, j);
		explicit.until(ExpectedConditions.visibilityOfElementLocated(locator)).click();
	}
	
	public static void explicittext(final WebDriver driver, int k, By locator, String name)
	{
		WebDriverWait explicittext = new WebDriverWait(driver, k);
		explicittext.until(ExpectedConditions.visibilityOfElementLocated(locator)).sendKeys(name);
	}
	
	public static void sele (final WebDriver driver, String text, By locator)
	{
		new Select(driver.findElement(locator)).selectByVisibleText(text);
	}
}
