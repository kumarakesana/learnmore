package utils.pack;

public class ApplicationConstants {
	
	public static final String Application_Properties_File = "application.properties";
	public static final String Property_Browser_Key = "browser.property.key";
	public static final String Empty_String = "";
	public static final String Chrome_Driver = "webdriver.chrome.driver";
	public static final String Firefox_Driver = "webdriver.gecko.driver";

}
