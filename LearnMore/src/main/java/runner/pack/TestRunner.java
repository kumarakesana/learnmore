package runner.pack;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"features"},
glue = {"com.step.def"},
format = {"pretty", "html:target/Reports"},
tags = {"@Test"},
monochrome = false
		)

public class TestRunner {

}
