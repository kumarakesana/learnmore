package com.step.def;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import object.repo.Locators;
import utils.pack.Browserfactory;
import utils.pack.GenericLibrary;

public class FeaturesClass {
	
	public static WebDriver driver;
	String url = "https://jqueryui.com/";
	
	@Before("@Test")
	public void setup()
	{
		driver = Browserfactory.getDriver();
	}
	
	@Given("^I will navigate to webpage$")
	public void i_will_navigate_to_webpage() throws Throwable {
	    
	    driver.navigate().to(url);
	}

	@When("^I enter \"([^\"]*)\"$")
	public void i_enter(String arg1) throws Throwable {
	   driver.findElement(By.partialLinkText(arg1)).click();
	}

	@Then("^I will see the action$")
	public void i_will_see_the_action() throws Throwable {
		
		driver.switchTo().frame(0);
		WebElement drag = driver.findElement(Locators.drag);
		
		Actions dragg = new Actions(driver);
		dragg.dragAndDropBy(drag, 100, 200).release().build().perform();
		GenericLibrary.implicit(driver, 10);
		driver.navigate().back();
		GenericLibrary.implicit(driver, 10);
	}
	
	@When("^I enter the \"([^\"]*)\"$")
	public void i_enter_the(String arg1) throws Throwable {
		driver.findElement(By.partialLinkText(arg1)).click();
	}

	@Then("^I will see drag and drop$")
	public void i_will_see_drag_and_drop() throws Throwable {
		driver.switchTo().frame(0);
		WebElement drag = driver.findElement(Locators.drag);
		WebElement drop = driver.findElement(Locators.drop);
		
		Actions dragg = new Actions(driver);
		dragg.dragAndDrop(drag, drop).release().build().perform();
		GenericLibrary.implicit(driver, 10);
		Thread.sleep(5000);
	}
	
	@After("@Test")
	public void teardown()
	{
		driver.quit();
	}

}
